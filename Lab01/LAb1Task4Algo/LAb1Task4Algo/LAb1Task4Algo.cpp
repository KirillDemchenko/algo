﻿

#include "pch.h"
#include <iostream>
#include <bitset>

using namespace std;
union TestFloat{
	struct {
		unsigned char bit0 : 1;
		unsigned char bit1 : 1;
		unsigned char bit2 : 1;
		unsigned char bit3 : 1;
		unsigned char bit4 : 1;
		unsigned char bit5 : 1;
		unsigned char bit6 : 1;
		unsigned char bit7 : 1;
		unsigned char bit8 : 1;
		unsigned char bit9 : 1;
		unsigned char bit10 : 1;
		unsigned char bit11 : 1;
		unsigned char bit12 : 1;
		unsigned char bit13 : 1;
		unsigned char bit14 : 1;
		unsigned char bit15 : 1;
		unsigned char bit16 : 1;
		unsigned char bit17 : 1;
		unsigned char bit18 : 1;
		unsigned char bit19 : 1;
		unsigned char bit20 : 1;
		unsigned char bit21 : 1;
		unsigned char bit22 : 1;
		unsigned char bit23 : 1;
		unsigned char bit24 : 1;
		unsigned char bit25 : 1;
		unsigned char bit26 : 1;
		unsigned char bit27 : 1;
		unsigned char bit28 : 1;
		unsigned char bit29 : 1;
		unsigned char bit30 : 1;
		unsigned char bit31 : 1;

	}bits;

	struct {
		unsigned char byte1 : 8;
		unsigned char byte2 : 8;
		unsigned char byte3 : 8;
		unsigned char byte4 : 8;
	}bytes;

	struct {
		unsigned int matissa : 23;
		unsigned int degree : 8;
		unsigned int sign : 1;
	}parts;

	float number;
};




int main()
{
	TestFloat float_number;
	cout << "enter your number: ";
	cin >> float_number.number;

	cout << "Size general nuber: " << sizeof(float_number.number) << endl;
	cout << "Size number by bit: " << sizeof(float_number.bits) << endl;
	cout << "Size number by bytes: " << sizeof(float_number.bytes) << endl;
	cout << "Size number by main parts: " << sizeof(float_number.parts) << endl;
	cout << endl;

	cout << (short)float_number.bits.bit31;
	cout << (short)float_number.bits.bit30;
	cout << (short)float_number.bits.bit29;
	cout << (short)float_number.bits.bit28;
	cout << (short)float_number.bits.bit27;
	cout << (short)float_number.bits.bit26;
	cout << (short)float_number.bits.bit25;
	cout << (short)float_number.bits.bit24;
	cout << (short)float_number.bits.bit23;
	cout << (short)float_number.bits.bit22;
	cout << (short)float_number.bits.bit21;
	cout << (short)float_number.bits.bit20;
	cout << (short)float_number.bits.bit19;
	cout << (short)float_number.bits.bit18;
	cout << (short)float_number.bits.bit17;
	cout << (short)float_number.bits.bit16;
	cout << (short)float_number.bits.bit15;
	cout << (short)float_number.bits.bit14;
	cout << (short)float_number.bits.bit13;
	cout << (short)float_number.bits.bit12;
	cout << (short)float_number.bits.bit11;
	cout << (short)float_number.bits.bit10;
	cout << (short)float_number.bits.bit9;
	cout << (short)float_number.bits.bit8;
	cout << (short)float_number.bits.bit7;
	cout << (short)float_number.bits.bit6;
	cout << (short)float_number.bits.bit5;
	cout << (short)float_number.bits.bit4;
	cout << (short)float_number.bits.bit3;
	cout << (short)float_number.bits.bit2;
	cout << (short)float_number.bits.bit1;
	cout << (short)float_number.bits.bit0;
	cout << endl;
	cout << endl;
	cout << endl;

	cout << "First byte: " << bitset<8>(float_number.bytes.byte4) << endl;
	cout << "Second byte: " << bitset<8>(float_number.bytes.byte3) << endl;
	cout << "Third byte: " << bitset<8>(float_number.bytes.byte2) << endl;
	cout << "Fourth byte: " << bitset<8>(float_number.bytes.byte1) << endl;
	cout << endl;

	float_number.parts.sign == 1 ? cout << "Sing: " << bitset<1>(float_number.parts.sign) << " (-)" << endl : cout << "Sing: " << bitset<1>(float_number.parts.sign) << " (+)" << endl;
	cout << "Degree: " << bitset<8>(float_number.parts.degree) << endl;
	cout << "Matissa: " << bitset<23>(float_number.parts.matissa) << endl;



	return 0;
}

