﻿

#include "pch.h"
#include <iostream>
#include <stdio.h>
#include <locale.h>
#include <ctime>
#include <Windows.h>
#include <bitset>

using namespace std;

struct time {
	unsigned short Minutes : 6;
	unsigned short Seconds : 6;
	unsigned short Month : 4;
	
	unsigned short Year : 5;
	unsigned short Hour : 5;
	unsigned short Day : 5;
}date;

union data16{
	struct Sign{
		unsigned short number : 15;
		unsigned short znak : 1;
	}bytes;
	signed short numb : 16;
}test;


int main()
{

	setlocale(LC_ALL, "rus");
	//cout << sizeof(time);
	int task;
	cout << "Enter numb of tusk: ";
	cin >> task;
	switch (task) {
	case 1: {
		SYSTEMTIME lt;

		GetLocalTime(&lt);

		date.Day = lt.wDay;
		date.Month = lt.wMonth;
		date.Year = lt.wYear % 2000;
		date.Hour = lt.wHour;
		date.Minutes = lt.wMinute;
		date.Seconds = lt.wSecond;

		printf("Local date is: %02d.%02d.20%02d\n", date.Day, date.Month, date.Year);
		printf("Local time is: %02d:%02d\n", date.Hour, date.Minutes);

		cout << "Size of date: " << sizeof(date) << "kb";


		cout << endl;
		break;
	}//+
	case 2: {
		signed short x2;
		cout << endl;
		cout << "Введите число для проверки знака: ";
		cin >> x2;

		test.numb = x2;
		cout << "Структура данных" << endl;
		cout << "Numb: " << 0 - test.numb << endl;
		cout << "Znak: " << test.bytes.znak;
		test.bytes.znak == 1 ? cout << " (-)" : cout << " (+)";

		cout << endl;
		cout << "Логическая операция" << endl;
		if (test.bytes.znak) {
			cout << "znak - ";
		}
		else {
			cout << "znak + ";
		}

		cout << endl;
		cout << "Арифметическая операция" << endl;
		if ((test.bytes.number + test.bytes.number) >= 0) {
			cout << "znak - ";
		}
		else {
			cout << "znak + ";
		}
		cout << endl;
		break;
	}
	case 3: {
		int ThirdTask;
		cout << "oper 1 - plus\noper 2 - minus\n\
oper 3 - and(&)\noper 4 - or(|)\n(unsigned char) - 5\n";

		cout << "Number of operation: ";
		cin >> ThirdTask;

		switch (ThirdTask) {
			case 1: {
				int num2, num1;
				signed char res;
				cout << "Num1: ";
				cin >> num1;
				cout << "Num2: ";
				cin >> num2;
				res = num1 + num2;
				cout << (short)res << endl;
				cout << bitset<20>(num1) << endl;
				cout << bitset<20>(num2) << endl;
				cout << bitset<20>(res) << endl;
				cout << endl;
				break;
			}
			case 2: {
				int num2, num1;
				signed char res;
				cout << "Num1: ";
				cin >> num1;
				cout << "Num2: ";
				cin >> num2;
				res = num1 - num2;
				cout << (short)res << endl;
				cout << bitset<20>(num1) << endl;
				cout << bitset<20>(num2) << endl;
				cout << bitset<20>(res) << endl;
				cout << endl;
				break;
			}
			case 3: {
				int num2, num1;
				signed char res;
				cout << "Num1: ";
				cin >> num1;
				cout << "Num2: ";
				cin >> num2;
				res = num1 & num2;
				cout << (short)res << endl;
				cout << bitset<20>(num1) << endl;
				cout << bitset<20>(num2) << endl;
				cout << bitset<20>(res) << endl;
				cout << endl;
				break;
			}
			case 4: {
				int num2, num1;
				signed char res;
				cout << "Num1: ";
				cin >> num1;
				cout << "Num2: ";
				cin >> num2;
				res = num1 | num2;
				cout << (short)res << endl;
				cout << bitset<20>(num1) << endl;
				cout << bitset<20>(num2) << endl;
				cout << bitset<20>(res) << endl;
				cout << endl;
				break;
			}
			case 5: {
				int num;
				signed char res;
				cout << "Num: ";
				cin >> num;
				res = (unsigned char)num;
				
				cout << (short)res << endl;
				cout << (int)(unsigned char)num << endl;
				cout << bitset<20>(num) << endl;
				cout << bitset<20>(res) << endl;
				cout << endl;
				break;
			}
		
		}






	}
	}
	return 0;
}


