﻿
#include "pch.h"
#include <iostream>
#include <ctime>
#include <clocale>

using namespace std;

int a = 1140671485;
int c = 12820163;
long long m = (long long)1 << 24;

int random() {
	static long long seed = time(NULL);
	seed = (a * seed + c) % m;
	return seed % 250;
}

int main()
{
	setlocale(LC_ALL, "rus");

	const int N = 2000;
	int array[N], count;

	double P, M, D, Q;

	for (int i = 0; i < N; i++) {
		array[i] = random();
	}
	
	// Розщитать частоту интервалов появления рандомных величин (интервал равен 1)
	for (int i = 0; i < 250; i++) {
		
		count = 0;
		for (int j = 0; j < N; j++) {
			if (array[j] == i) {
				count++;
			}
		}
		cout << "Число " << i << ":" << endl;
		cout << "Частота интервала " << count << " раз" << endl;
		cout << "--------------------------" << endl;
	}

	P = double(count) / N;
	cout << "Частота интервалов появления ранд. величин. Р = " << P <<endl;

	//Розщитать математическое сподивання ранд величин
	M = 0;
	for (int i = 0; i < N; i++) {
		M += array[i] * P;
	}
	cout << "математическое сподивання ранд величин. M = " << M << endl;
	//Розщитать дисперсию ранд величин

	D = 0;
	for (int i = 0; i < N; i++) {
		D += pow((array[i] - M), 2)*P;
	}

	cout << "дисперсия ранд величин. D = " << D << endl;

	//Розщитать средньоквадратическое отклонение ранд величин
	Q = sqrt(D);
	cout << "средньоквадратическое отклонение ранд величин. Q = " << Q << endl;

	
	return 0;
}

