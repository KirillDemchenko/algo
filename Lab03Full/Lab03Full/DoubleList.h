#pragma once
#include "pch.h"
#include <string>
#include <locale.h>

using namespace std;

template<typename T>
class DoubleList {
public:
	DoubleList();
	~DoubleList();

	void push_front(T data);
	void push_back(T data);
	void pop_front();
	void pop_back();
	void insert(T data, int index);
	void removeAT(int index);

	T& operator[] (const int index);

	int GetSize() { return Size; }
	void clear();

private:
	template<typename T>
	class Node
	{
	public:
		Node *pNext;
		Node *pPrev;
		T data;
		Node(T data = T(), Node *pPrev = nullptr, Node *pNext = nullptr) {
			this->data = data;
			this->pNext = pNext;
			this->pPrev = pPrev;
		}
	};
	int Size;
	Node<T> *head;
	Node<T> *tail;
	T delElem;
};




template<typename T>
DoubleList<T>::DoubleList()
{
	Size = 0;
	head = nullptr;
	tail = nullptr;
}

template<typename T>
DoubleList<T>::~DoubleList()
{
	clear();
}


template<typename T>
void DoubleList<T>::push_front(T data)
{
	if (this->head == nullptr) {
		this->head = new Node<T>(data);
		this->tail = this->head;
	}
	else {
		this->head->pPrev = new Node<T>(data, nullptr > this->head);
		this->head = this->head->pPrev;
	}
	Size++;
}

template<typename T>
void DoubleList<T>::push_back(T data)
{
	if (this->tail == nullptr) {
		this->head = new Node<T>(data);
		this->tail = this->head;
	}
	else {
		this->tail->pNext = new Node<T>(data, this->tail);
		this->tail = this->tail->pNext;
	}
	Size++;
}

template<typename T>
void DoubleList<T>::pop_front()
{
	Node<T> *temp = this->head;
	this->head = this->head->pNext;
	if (this->head == nullptr) {
		this->tail = nullptr;
	}
	else {
		this->head->pPrev = nullptr;
	}
	this->delElem = temp->data;
	delete temp;
	Size--;

}

template<typename T>
void DoubleList<T>::pop_back()
{
	Node<T> *temp = this->tail;
	this->tail = this->tail->pPrev;
	if (this->tail == nullptr) {
		this->head = nullptr;
	}
	else {
		this->tail->pNext = nullptr;
	}
	this->delElem = temp->data;
	delete temp;
	Size--;

}

template<typename T>
void DoubleList<T>::insert(T data, int index)
{
	if (index == 0) {
		this->push_back(data);
	}
	else if (index == Size) {
		this->push_back(data);
	}
	else {
		Node<T> * current;
		if (index < Size / 2) {
			current = this->tail;
			int i = Size - 1;
			for (; i > index; i--) {
				current = current->pPrev;
			}
		}
		else {
			current = this->tail;
			int i = Size - 1;
			for (; i > index; i--) {
				current = current->pPrev;
			}
		}
		current->pPrev = new Node<T>(data, current->pPrev, current);
		current->pPrev->pPrev->pNext = current->pPrev;
		Size++;
	}

}

template<typename T>
void DoubleList<T>::removeAT(int index)
{
	if (index == 0) {
		this->pop_front();
	}
	else if (index == Size - 1) {
		this->pop_back();
	}
	else {
		Node<T> *current;
		if (index < Size / 2) {
			current = this->head;
			for (int i = 0; i < index; i++) {
				current = current->pNext;
			}
		}
		else {
			current = this->tail;
			int i = Size - 1;
			for (; i > index; i--) {
				current = current->pPrev;
			}
		}
		current->pPrev->pNext = current->pNext;
		current->pNext->pPrev = current->pPrev;
		this->delElem = current->data;
		delete current;
		Size--;
	}

}

template<typename T>
T & DoubleList<T>::operator[](const int index)
{
	Node<T> *current;
	if (index < Size / 2) {
		current = this->head;
		for (int i = 0; i < index; i++) {
			current = current->pNext;
		}
	}
	else {
		current = this->tail;
		int i = Size - 1;
		for (; i > index; i--) {
			current = current->pPrev;
		}
	}
}

template<typename T>
void DoubleList<T>::clear()
{
	while (Size) {
		pop_front();
	}

}
