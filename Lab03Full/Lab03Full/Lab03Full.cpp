﻿

#include "pch.h"
#include <iostream>
#include "SinglList.h"
#include "DoubleList.h"
#include "Turn.h"
#include "Stack.h"

using namespace std;


void GetList() {
	List<int> lst;
	for (int i = 0; i < lst.GestSize(); i++) {
		cout << lst[i] << " ";
	}
}

int main()
{
	setlocale(LC_ALL, "rus");
	int task=0;
	List<int> singList;
	while (task != 9) {
		cout << "Выберете динамическую структуру данных: " << endl;
		cout << "1 - Односвязный список\n" << "2 - Двусвязный список\n" << "3 - Стек\n" << "4 - Очередь\n\n";
		cin >> task;
		switch (task) {
		case 1: {
			int numb;
			cout << "Односвязный список" << endl << endl;
			cout << "Выберете операцию: " << endl;
			cout << "1 - Добавить в начало\n" << "2 - Добавить в конец\n" << "3 - Удалить с начала\n" << "4 - Удалить с конца\n" <<\
				 "5 - Добавить по индексу\n" << "6 - Удалить по индексу\n" << "7 - Удалить все елементы\n" << "8 - Размер списка\n" << "9 - Вывести список\n" << "10 - Удалить список\n";
			cin >> numb;
			switch (numb) {
			case 1: {
				int count;
				cout << "Введите значение которое хотите добавить в начало: "<<endl<<endl;
				cin >> count;
				singList.push_front(count);
				break;
			}
			case 2: {
				int count;
				cout << "Введите значение которое хотите добавить в конец: "<<endl << endl;
				cin >> count;
				singList.push_back(count);
				break;
			}
			case 3: {
				singList.pop_front();
				cout << "Елемент в начале удален: " << endl << endl;
				break;
			}
			case 4: {
				singList.pop_back();
				cout << "Елемент в конце удален: " << endl << endl;
				break;
			}
			case 5: {
				int x1, x2;
				cout << "Введите число которое хотите добавить: ";
				cin >> x1;
				cout << "Введите число индекс: ";
				cin >> x2;
				cout << endl;
				singList.insert(x1, x2);
				cout << "Значение "<<x1<<" добавлено на место " << x2;
				cout << endl;
				break;
			}
			case 6: {
				int ind;
				cout << "Введите индекс числа которое хотите удалить: ";
				cin >> ind;
				if(ind <= singList.GestSize()){
					singList.removeAT(ind);
					cout << "Значение " << singList[ind] << "удалено из позиции " << ind;
				}
				else {
					cout << "не верно введены данные" << endl;
				}
				break;
			}
			case 7: {
				singList.clear();
				cout << "Все елементы удалены из списка"<<endl;
				break;
			}
			case 8: {
				cout << "Размер списка: "<<singList.GestSize()<<endl;
				break;
			}
			case 9: {
				cout << "Вывод списка: " << endl;
				for (int i = 0; i < singList.GestSize(); i++) {
					cout << singList[i] << " ";
				}

				cout << endl;
				cout << endl;
				break;
			}
			case 10: {
				singList.clear();
				cout << "Все елементы удалены из списка" << endl;
				break;
			}







					break;
			}
		}
		}
	}
	return 0;
}

