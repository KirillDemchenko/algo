#pragma once
#include "pch.h"
#include <iostream>
#include <locale.h>

template<typename T>
class Stack {
public:
	Stack();
	~Stack();
	void push(T data);
	T& pop();
	void clear();
	int getSize() { return Size; }
	T& peek();

private:
	template<typename T>
	class Node {
	public:
		Node *pNext;
		T data;
		Node(T data = T(), Node *pNext = nullptr) {
			this->data = data;
			this->pNext = pNext;
		}
	};


	int Size;
	Node<T> *head;
	T delElem;
};


template<typename T>
Stack<T>::Stack()
{
	Size = 0;
	head = nullptr;

}

template<typename T>
Stack<T>::~Stack()
{
	clear();
}

template<typename T>
void Stack<T>::push(T data)
{
	this->head = new Node<T>(data, this->head);
	Size++;
}

template<typename T>
T & Stack<T>::pop()
{
	Node<T> *temp = this->head;
	this->head = this->head->pNext;
	this->delElem = temp->data;
	delete temp;
	Size--;
}

template<typename T>
void Stack<T>::clear()
{
	while (Size) {
		pop();
	}
}

template<typename T>
T & Stack<T>::peek()
{
	return this->head->data;
}
