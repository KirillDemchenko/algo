#pragma once
#include "pch.h"
#include <iostream>
#include <locale.h>

template<typename T>
class Turn
{
public:
	Turn();
	~Turn();

	void push(T data);
	T& pop();

	void clear();
	int GetSize() { return Size; }

	T& getHead();
	T& getTail();

private:
	template<typename T>
	class Node
	{
		Node *pNext;
		Node *pPrev;
		T data;
		Node(T data = T(), Node *pPrev = nullptr, Node *pNext = nullptr) {
			this->data = data;
			this->pNext = pNext;
			this->pPrev = pPrev;
		}

	};
	int Size;
	Node<T> *head;
	Node<T> *tail;
	T delElem;
};






template<typename T>
Turn<T>::Turn()
{
	Size = 0;
	head = nullptr;
	tail = nullptr;
}

template<typename T>
Turn<T>::~Turn()
{
	clear();
}

template<typename T>
void Turn<T>::push(T data)
{
	if (this->tail == nullptr) {
		this->head = new Node<T>(data);
		this->tail = this->head;
	}
	else {
		this->tail->pNext = new Node<T>(data, this->tail);
		this.tail = this->tail->pNext;
	}
	Size++;
}

template<typename T>
T & Turn<T>::pop()
{
	Node<T> *temp = this->head;
	this->head = this->head->pNext;
	if (this->head == nullptr) {
		this->tail = nullptr;
	}
	else {
		this->head->pPrev = nullptr;
	}
	this->delElem = temp->data;
	delete temp;
	Size--;
}

template<typename T>
void Turn<T>::clear()
{
	while (Size) {
		pop();
	}
}

template<typename T>
T & Turn<T>::getHead()
{
	return this->head->data;
}

template<typename T>
T & Turn<T>::getTail()
{
	return this->tail->data;
}